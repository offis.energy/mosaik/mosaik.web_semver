mosaik-web
==========

A simple mosaik simulation visualization for web browsers.


Status
------

.. image:: https://gitlab.com/offis.energy/mosaik/mosaik.api-python_semver/badges/master/pipeline.svg
    :target: https://gitlab.com/offis.energy/mosaik/mosaik.api-python_semver/-/jobs
    :alt: Pipeline status

.. image:: https://gitlab.com/offis.energy/mosaik/mosaik.api-python_semver/badges/master/coverage.svg
    :target: https://gitlab.com/offis.energy/mosaik/mosaik.api-python_semver/commits/master
    :alt: Coverage Report

.. image:: https://img.shields.io/pypi/l/mosaik.Web_SemVer.svg
    :target: https://gitlab.com/offis.energy/mosaik/mosaik.api-python_semver/blob/master/LICENSE.txt
    :alt: PyPI - License

.. image:: https://img.shields.io/pypi/status/mosaik-api_semver.svg
    :target: https://pypi.org/project/mosaik.Web_SemVer/
    :alt: PyPI - Status

.. image:: https://img.shields.io/pypi/v/mosaik.Web_SemVer
    :target: https://pypi.org/project/mosaik.Web_SemVer/#history
    :alt: PyPI version

.. image:: https://img.shields.io/librariesio/release/pypi/mosaik.Web_SemVer
    :target: https://libraries.io/pypi/mosaik.Web_SemVer
    :alt: Libraries status

.. image:: https://img.shields.io/pypi/pyversions/mosaik.Web_SemVer
    :target: https://pypi.org/project/mosaik.Web_SemVer/
    :alt: Python Versions

Installation
------------

::

    $ pip install mosaik-web
